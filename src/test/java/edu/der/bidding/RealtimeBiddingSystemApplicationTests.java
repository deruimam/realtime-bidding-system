package edu.der.bidding;

import com.fasterxml.jackson.core.JsonProcessingException;
import edu.der.bidding.model.Item;
import edu.der.bidding.model.UserBid;
import edu.der.bidding.properties.KafkaProperties;
import edu.der.bidding.service.KafkaProducerService;
import edu.der.bidding.util.ModelUtil;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.KStream;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;
import java.util.Properties;
import java.util.Random;
import java.util.concurrent.TimeUnit;

@SpringBootTest
@TestMethodOrder(value = MethodOrderer.OrderAnnotation.class)
class RealtimeBiddingSystemApplicationTests {

	@Autowired(required = false)
	private KafkaProducerService producerService;

	@Autowired(required = false)
	private KafkaProperties kafkaProps;

	@Test
	@Order(1)
	void testProduceData() throws JsonProcessingException {
		long closeBidTimestamp = new Date().getTime() + TimeUnit.SECONDS.toSeconds(10);
		Item item = new Item("1", "New Balance V1", 88_000L, closeBidTimestamp);
		producerService.sendAsCallable("source_topic_1", null, ModelUtil.getMapperObjectToJsonString(item));
	}

	@Disabled
	@Test
	@Order(2)
	void testProduceData2() throws JsonProcessingException {
		long closeBidTimestamp = 1688965200000L;
		Item item = new Item("1", "New Balance V2", 88_000L, closeBidTimestamp);
		producerService.sendAsCallable("source_topic_2", null, ModelUtil.getMapperObjectToJsonString(item));
	}

	@Disabled
	@Test
	@Order(3)
	void testStreamData1(){
		StreamsBuilder streamsBuilder = new StreamsBuilder();

		final Serde<String> STRING_SERDE = Serdes.String();

		KStream<String, String> messageStream = streamsBuilder
				.stream("source_topic", Consumed.with(STRING_SERDE, STRING_SERDE));

		messageStream.foreach((key, value) -> {
			Item item = new Item();

			try {
				item = ModelUtil.getMapperJsonStringToObject(value, Item.class);
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			}

			if (item.getIsClosedBid() != true){
				for (int i = 1; i <= 7; i++) {
					String userId = String.valueOf(i);
					String userName = "User "+i;
					String userItemName = item.getName();
					Long totalBid = Long.valueOf(String.valueOf(new Random().longs(88_000L, 1_000_000)));
					long bidTimestamp = new Date().getTime();

					UserBid userBid = new UserBid(userId, userName, userItemName, Long.valueOf(totalBid), bidTimestamp);
					try {
						producerService.sendAsCallable("user_bid", null, ModelUtil.getMapperObjectToJsonString(userBid));
					} catch (JsonProcessingException e) {
						e.printStackTrace();
					}

					try {
						Thread.sleep(TimeUnit.SECONDS.toSeconds(2));
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		});

	}

	@Disabled
	@Test
	@Order(4)
	void testStreamData2() {
		StreamsBuilder builder = new StreamsBuilder();

		KStream<String, String> stream = builder.stream("source_topic");

		final Properties props = new Properties();
		props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaProps.getKafkaBrokerServers());
		props.put(StreamsConfig.APPLICATION_ID_CONFIG, this.getClass().getName() + "-test");
		props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG,	StringDeserializer.class.getName());
		props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, StringDeserializer.class.getName());

		KafkaStreams streams = new KafkaStreams(builder.build(), props);
		streams.start();

		try {
			while (true){
				stream.foreach(
					(key, value) -> {
						try {
							Item item = ModelUtil.getMapperJsonStringToObject(value, Item.class);

							if (item.getIsClosedBid() != true){
								for (int i = 1; i <= 7; i++) {
									String userId = String.valueOf(i);
									String userName = "User "+i;
									String userItemName = item.getName();
									Long totalBid = Long.valueOf(String.valueOf(new Random().longs(88_000L, 1_000_000)));
									long bidTimestamp = new Date().getTime();

									UserBid userBid = new UserBid(userId, userName, userItemName, Long.valueOf(totalBid), bidTimestamp);
									producerService.sendAsCallable("user_bid", null, ModelUtil.getMapperObjectToJsonString(userBid));

									Thread.sleep(TimeUnit.SECONDS.toSeconds(2));
								}
							}
						} catch (JsonProcessingException | InterruptedException e) {
							e.printStackTrace();
						}
					});
			}
		} finally {
			// close Kafka Streams when the JVM shuts down.
			Runtime.getRuntime().addShutdownHook(new Thread(streams::close));
		}
	}

}
