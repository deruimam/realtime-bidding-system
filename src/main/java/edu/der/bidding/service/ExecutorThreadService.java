package edu.der.bidding.service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.annotation.PostConstruct;

import edu.der.bidding.properties.KafkaProperties;
import edu.der.bidding.stream.KafkaStream;
import edu.der.bidding.stream.StreamConsumer;
import edu.der.bidding.stream.StreamHigherBidConsumer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@ConditionalOnProperty(
        name = "internal.prop.enable-executor",
        havingValue = "true",
        matchIfMissing = false
)
public class ExecutorThreadService {

    @Autowired(required = false)
    private KafkaProperties kafkaProps;

    @Autowired(required = false)
    private KafkaProducerService kafkaProducerService;

    @PostConstruct
    public void init() {
        this.startExecutor();
    }

    public void startExecutor() {
        StreamConsumer thread1 = new StreamConsumer(kafkaProps, kafkaProducerService);
        StreamHigherBidConsumer thread2 = new StreamHigherBidConsumer(kafkaProps, kafkaProducerService);
//        KafkaStream thread3 = new KafkaStream(kafkaProps, kafkaProducerService);

        List<Runnable> threads = new ArrayList<>();
        threads.add(thread1);
        threads.add(thread2);

        int threadSize = threads.size();
        if(threadSize > 0) {
            /*Set max thread*/
            ExecutorService executor = Executors.newFixedThreadPool(threadSize);
            for(Runnable schedulerRunner: threads) {
                executor.submit(schedulerRunner);
            }
        }
        log.info("Executed!");
    }
}
