package edu.der.bidding.service;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class KafkaProducerService {

    @Autowired(required = false)
    private KafkaProducer<String, String> kafkaProducer;

    public void sendAsCallable(String topic, String key, String value) {
        if(kafkaProducer != null) {
            ProducerRecord<String, String> record = new ProducerRecord<>(topic, key, value);
            kafkaProducer.send(record, (metadata, exception) -> {
                String response = String.format("Topic=%s, Partition=%s, offset=%s", metadata.topic(), metadata.offset(), metadata.partition());
                log.info("key={}, value={}, response={} ", key, value, response);
            });
        }
    }

    public void sendAsVoid(String topic, String key, String value) {
        if(kafkaProducer != null) {
            ProducerRecord<String, String> record = new ProducerRecord<>(topic, key, value);
            kafkaProducer.send(record, null);
        }
    }
}
