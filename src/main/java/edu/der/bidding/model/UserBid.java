package edu.der.bidding.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import edu.der.bidding.util.ModelUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserBid {

    protected String userId;
    protected String userName;
    protected String userItemName;
    protected Long totalBid;
    protected Long bidTimestamp;

    public String getUserBidInHuman(){
        String dateTime = "";
        if(bidTimestamp != null && bidTimestamp > 0) {
            dateTime = ModelUtil.timestampToString(bidTimestamp, ModelUtil.FORMATTER, ModelUtil.GMT_PLUS_7);
        }
        return dateTime;
    }
}
