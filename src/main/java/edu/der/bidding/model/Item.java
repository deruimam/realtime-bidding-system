package edu.der.bidding.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import edu.der.bidding.util.ModelUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Item {

    protected String id;
    protected String name;
    protected Long price;

    protected Long closeBidTimestamp;

    public String getCloseBidInHuman(){
        String dateTime = "";
        if(closeBidTimestamp != null && closeBidTimestamp > 0) {
            dateTime = ModelUtil.timestampToString(closeBidTimestamp, ModelUtil.FORMATTER, ModelUtil.GMT_PLUS_7);
        }
        return dateTime;
    }

    public boolean getIsClosedBid(){
        boolean isClosed = false;
        long dateCheckTimestamp = new Date().getTime();
        if (dateCheckTimestamp > closeBidTimestamp){
            isClosed = true;
        }
        return isClosed;
    }
}
