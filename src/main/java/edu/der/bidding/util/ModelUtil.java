package edu.der.bidding.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.ThreadLocalRandom;

@Slf4j
public class ModelUtil {

    public static final String FORMATTER = "yyyy-MM-dd HH:mm:ss";
    public static final String GMT_PLUS_7 = "Antarctica/Davis";

    public static String getMapperObjectToJsonString(Object obj) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(obj);
    }

    public static <T> T getMapperJsonStringToObject(String content, Class<T> valueType) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(content, valueType);
    }

    public static String timestampToString(long timestamp, String format, String timezoneId) {
        String dateString = null;
        Date date = new Date(timestamp);
        if (date != null && StringUtils.isNotBlank(format) && StringUtils.isNotBlank(timezoneId)) {
            dateString = dateToString(date, format, timezoneId);
        }
        return dateString;
    }

    public static String dateToString(Date date, String format, String timezoneId) {
        String dateString = null;
        if (date != null && StringUtils.isNotBlank(format) && StringUtils.isNotBlank(timezoneId)) {
            DateFormat formater = new SimpleDateFormat(format);
            formater.setTimeZone(TimeZone.getTimeZone(timezoneId));
            dateString =  formater.format(date);
        }
        return dateString;
    }

    public static long nextLongBetween(long min, long max) {
        return ThreadLocalRandom.current().nextLong(min, max);
    }
}
