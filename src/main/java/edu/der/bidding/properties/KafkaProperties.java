package edu.der.bidding.properties;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Setter
@Getter
@Configuration
@ConfigurationProperties(prefix = "internal.prop")
@JsonIgnoreProperties(ignoreUnknown = true)
public class KafkaProperties {
    @Value("${server.port}")
    private String serverPort;

    @Value("${internal.prop.kafka-broker-servers}")
    private String kafkaBrokerServers;
}
