package edu.der.bidding.config;

import edu.der.bidding.properties.KafkaProperties;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.Properties;

@Slf4j
@Configuration
@ConditionalOnProperty(
        name = "internal.prop.enable-kafka-producer",
        havingValue = "true",
        matchIfMissing = false
)
public class KafkaProducerConfig {

    @Autowired(required = false)
    private KafkaProperties serviceProps;

    private KafkaProducer<String, String> kafkaProducer;

    @PostConstruct
    public void initProducer(){
        Properties props = new Properties();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, serviceProps.getKafkaBrokerServers());
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        kafkaProducer = new KafkaProducer<>(props);
    }

    @Bean
    public KafkaProducer<String, String> kafkaProducer() {
        return kafkaProducer;
    }

    @PreDestroy
    public void destroyProducer() {
        kafkaProducer.close();
        log.info("Kafka Producer Destroyed!");
    }
}
