package edu.der.bidding.stream;

import com.fasterxml.jackson.core.JsonProcessingException;
import edu.der.bidding.model.UserBid;
import edu.der.bidding.properties.KafkaProperties;
import edu.der.bidding.service.KafkaProducerService;
import edu.der.bidding.util.ModelUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.Duration;
import java.util.*;

@Slf4j
@Setter
@Getter
@AllArgsConstructor
public class StreamHigherBidConsumer implements Runnable{
    private KafkaProperties serviceProps;

    @Autowired(required = false)
    private KafkaProducerService producerService;

    private Consumer<String, String> createConsumer(){
        final Properties props = new Properties();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, serviceProps.getKafkaBrokerServers());
        props.put(ConsumerConfig.GROUP_ID_CONFIG, this.getClass().getName() + "-dev");
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG,	StringDeserializer.class.getName());
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());

        // Create the consumer.
        final Consumer<String, String> consumer = new KafkaConsumer<>(props);

        // Subscribe to the topic.
        List<String> topics = new ArrayList<>();
        topics.add("user_bid");
        consumer.subscribe(topics);
        return consumer;
    }

    public void startConsume(){
        final Consumer<String, String> consumer = createConsumer();
        log.info("Topics to be consumed = {}", consumer.listTopics().keySet());
        try {
            while (true){
                final ConsumerRecords<String, String> consumerRecords = consumer.poll(Duration.ofHours(1));
                List<UserBid> userBids = new ArrayList<>();
                consumerRecords.forEach(record -> {
                    try {
                        UserBid userBid = ModelUtil.getMapperJsonStringToObject(record.value(), UserBid.class);
                        userBids.add(userBid);
                    } catch (Exception e) {
                        log.error("Error: ",e);
                    }
                });
                consumer.commitAsync();

                /*find the winner*/
                UserBid theWinner = userBids.stream()
                        .max(Comparator.comparing(UserBid::getTotalBid))
                        .orElseThrow(NoSuchElementException::new);

                /*publish the winner*/
                producerService.sendAsCallable("user_bid_winner", null, ModelUtil.getMapperObjectToJsonString(theWinner));
            }
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } finally {
            consumer.close();
        }
    }

    @Override
    public void run() {
        startConsume();
    }
}
