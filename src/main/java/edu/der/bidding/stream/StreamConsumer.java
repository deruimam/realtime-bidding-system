package edu.der.bidding.stream;

import edu.der.bidding.model.Item;
import edu.der.bidding.model.UserBid;
import edu.der.bidding.properties.KafkaProperties;
import edu.der.bidding.service.KafkaProducerService;
import edu.der.bidding.util.ModelUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.Duration;
import java.util.*;

@Slf4j
@Setter
@Getter
@AllArgsConstructor
public class StreamConsumer implements Runnable{

    private KafkaProperties serviceProps;

    @Autowired(required = false)
    private KafkaProducerService producerService;

    private Consumer<String, String> createConsumer(){
        final Properties props = new Properties();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, serviceProps.getKafkaBrokerServers());
        props.put(ConsumerConfig.GROUP_ID_CONFIG, this.getClass().getName() + "-dev");
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG,	StringDeserializer.class.getName());
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());

        // Create the consumer.
        final Consumer<String, String> consumer = new KafkaConsumer<>(props);

        // Subscribe to the topic.
        List<String> topics = new ArrayList<>();
        topics.add("source_topic_1");
        consumer.subscribe(topics);
        return consumer;
    }

    public void startConsume(){
        final Consumer<String, String> consumer = createConsumer();
        log.info("Topics to be consumed={}", consumer.listTopics().keySet());
        try {
            while (true){
                final ConsumerRecords<String, String> consumerRecords = consumer.poll(Duration.ofHours(1));
                consumerRecords.forEach(record -> {
                    try {
                        Item item = ModelUtil.getMapperJsonStringToObject(record.value(), Item.class);
                        if (item.getIsClosedBid() != true){
                            for (int i = 1; i <= 7; i++) {
                                String userId = String.valueOf(i);
                                String userName = "User "+i;
                                String userItemName = item.getName();
                                long totalBid = ModelUtil.nextLongBetween(88_000L, 1_000_000);
                                long bidTimestamp = new Date().getTime();

                                UserBid userBid = new UserBid(userId, userName, userItemName, Long.valueOf(totalBid), bidTimestamp);
                                producerService.sendAsCallable("user_bid", null, ModelUtil.getMapperObjectToJsonString(userBid));
                            }
                        }
                    } catch (Exception e) {
                        log.error("Error: ",e);
                    }
                });
                consumer.commitAsync();
            }

        } finally {
            consumer.close();
        }
    }

    @Override
    public void run() {
        startConsume();
    }
}
