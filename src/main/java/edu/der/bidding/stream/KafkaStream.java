package edu.der.bidding.stream;

import com.fasterxml.jackson.core.JsonProcessingException;
import edu.der.bidding.model.Item;
import edu.der.bidding.model.UserBid;
import edu.der.bidding.properties.KafkaProperties;
import edu.der.bidding.service.KafkaProducerService;
import edu.der.bidding.util.ModelUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.KStream;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.*;
import java.util.concurrent.TimeUnit;

@Slf4j
@Setter
@Getter
@AllArgsConstructor
public class KafkaStream implements Runnable{
    private static final Serde<String> STRING_SERDE = Serdes.String();

    private KafkaProperties serviceProps;

    @Autowired(required = false)
    private KafkaProducerService producerService;

    void streamData() {
        StreamsBuilder builder = new StreamsBuilder();

        KStream<String, String> stream = builder.stream("source_topic_2");

        final Properties props = new Properties();
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, serviceProps.getKafkaBrokerServers());
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, this.getClass().getName());
        props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG,	StringDeserializer.class.getName());
        props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, StringDeserializer.class.getName());

        KafkaStreams streams = new KafkaStreams(builder.build(), props);
        streams.start();

        try {
            while (true){
                stream.foreach(
                        (key, value) -> {
                            try {
                                Item item = ModelUtil.getMapperJsonStringToObject(value, Item.class);

                                if (item.getIsClosedBid() != true){
                                    for (int i = 1; i <= 7; i++) {
                                        String userId = String.valueOf(i);
                                        String userName = "User "+i;
                                        String userItemName = item.getName();
                                        long totalBid = ModelUtil.nextLongBetween(88_000L, 1_000_000);
                                        long bidTimestamp = new Date().getTime();

                                        UserBid userBid = new UserBid(userId, userName, userItemName, Long.valueOf(totalBid), bidTimestamp);
                                        producerService.sendAsCallable("user_bid_2", null, ModelUtil.getMapperObjectToJsonString(userBid));

                                        Thread.sleep(TimeUnit.SECONDS.toSeconds(2));
                                    }
                                }
                            } catch (JsonProcessingException | InterruptedException e) {
                                e.printStackTrace();
                            }
                        });
            }
        } finally {
            // close Kafka Streams when the JVM shuts down.
            Runtime.getRuntime().addShutdownHook(new Thread(streams::close));
        }
    }

    private KafkaStreams createKafkaStream(){
        final Properties props = new Properties();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, serviceProps.getKafkaBrokerServers());
        props.put(ConsumerConfig.GROUP_ID_CONFIG, this.getClass().getName());
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG,	StringDeserializer.class.getName());
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());

        StreamsBuilder builder = new StreamsBuilder();
        KafkaStreams streams = new KafkaStreams(builder.build(), props);

        KStream<Void, String> stream = builder.stream("source_topic");
        return streams;
    }

    public void buildPipeline() {
        StreamsBuilder builder = new StreamsBuilder();

        KStream<String, String> stream = builder.stream("source_topic");

        final Properties props = new Properties();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, serviceProps.getKafkaBrokerServers());
        props.put(ConsumerConfig.GROUP_ID_CONFIG, this.getClass().getName());
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG,	StringDeserializer.class.getName());
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());

        KafkaStreams streams = new KafkaStreams(builder.build(), props);

        try {
            while (true){
                stream.foreach(
                        (key, value) -> {
                            try {
                                Item item = ModelUtil.getMapperJsonStringToObject(value, Item.class);

                                if (item.getIsClosedBid()){
                                    for (int i = 1; i <= 7; i++) {
                                        String userId = String.valueOf(i);
                                        String userName = "User "+i;
                                        String userItemName = item.getName();
                                        Long totalBid = Long.valueOf(String.valueOf(new Random().longs(88_000L, 1_000_000)));
                                        long bidTimestamp = new Date().getTime();

                                        UserBid userBid = new UserBid(userId, userName, userItemName, Long.valueOf(totalBid), bidTimestamp);
                                        producerService.sendAsCallable("user_bid", null, ModelUtil.getMapperObjectToJsonString(userBid));
                                        System.out.println("(DSL) Hello, " + userName);

                                        Thread.sleep(TimeUnit.SECONDS.toSeconds(2));
                                    }
                                }
                            } catch (JsonProcessingException | InterruptedException e) {
                                e.printStackTrace();
                            }
                        });

                streams.start();
            }
        } finally {
            // close Kafka Streams when the JVM shuts down.
            Runtime.getRuntime().addShutdownHook(new Thread(streams::close));
        }

//        KStream<String, String> messageStream = builder
//                .stream("source_topic", Consumed.with(STRING_SERDE, STRING_SERDE));

//        KTable<String, Item> wordCounts = messageStream
//                .filter()


//        wordCounts.toStream().to("output-topic");
    }

    @Override
    public void run() {
        streamData();
    }

}
