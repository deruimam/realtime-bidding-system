package edu.der.bidding;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RealtimeBiddingSystemApplication {

	public static void main(String[] args) {
		SpringApplication.run(RealtimeBiddingSystemApplication.class, args);
	}

}
