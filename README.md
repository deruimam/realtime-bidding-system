# Realtime Bidding System #

Realtime bidding system is a service that is used to find the winner of the bidding system through a message broker. This service was created as a form of research and development on the use of message broker technology.

```
Flow data:
    1. Add an Item through Kafka Producer.
    2. User bid the Item.
    3. Kafka Consume/Stream data and find the highest bid.
    4. Save the data through kafka producer in user_bid_winner topic.
```
